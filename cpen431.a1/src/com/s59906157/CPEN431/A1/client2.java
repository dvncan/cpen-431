package com.s59906157.CPEN431.A1;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Arrays;

import javax.xml.bind.DatatypeConverter;


public class client2{

	public static void main(String[] args){

		DatagramSocket s = null;
		DatagramPacket reply = null;
		
		int randomPIN = (int)(Math.random()*9000)+1000;
		//1381632
		int sNum = 59906157;
		int sPort = 5627;

		byte[] sMsg = new byte[128];
		byte[] rMsg = new byte[128];
		
		boolean pktRecieved = false;
		int counter = 0;
		int timeout = 1000;

		byte[] replyArray = new byte[128];
		
		try{
			while(counter < 3 && (pktRecieved == false)){
				
				//creating socket & setting addresses
				s = new DatagramSocket();
				InetAddress homeAddr = InetAddress.getByName("154.5.171.209");
				InetAddress sendAddr = InetAddress.getByName("162.219.6.226");			

				//setting header
				byte[] ipAddress = setByteAddr(4, homeAddr);
				byte[] port = set2Byte(sPort);
				byte[] pin = set2Byte(randomPIN);
				byte[] time = setByteTime(8, System.currentTimeMillis());

				//setting number
				byte[] studNum = ByteBuffer.allocate(4).putInt(sNum).array();

				//setting message
				sMsg = combineBytes(sMsg, studNum, ipAddress, port, pin, time);

				System.out.println(sMsg.length + "");
				//set packet
				DatagramPacket packet = new DatagramPacket(sMsg, sMsg.length, sendAddr, sPort);

				//send packet
				s.send(packet);

				//recieve packet reply
				reply = new DatagramPacket(rMsg, rMsg.length);

				try{

					s.setSoTimeout(timeout);
					s.receive(reply);
					pktRecieved = true;

				}catch(SocketTimeoutException e){

					timeout = (timeout*2);
					counter++;

				}
				
				
			
			}
		}catch(Exception e){

		}
		
		s.close();
		
		replyArray = reply.getData();
		
		String replyHex = byteArrayToHex(replyArray);
		
		ByteBuffer buffer = ByteBuffer.wrap(replyArray);
		
		int hexNum = ByteOrder.leb2int(replyArray, 16);
		
		System.out.println(replyHex);
		System.out.println(hexNum + "");
		
		
		
	}

	private static byte[] combineBytes(byte[] sMsg, byte[] studNum, byte[] ipAddress, byte[] port, byte[] pin, byte[] time) {

		System.arraycopy(ipAddress, 0, sMsg, 0, ipAddress.length);
		System.arraycopy(port, 0, sMsg, 4, port.length);
		System.arraycopy(pin, 0, sMsg, 6, pin.length);
		System.arraycopy(time, 0, sMsg, 8, time.length);
		System.arraycopy(studNum, 0, sMsg, 16, studNum.length);

		return sMsg;
	}

	public static byte[] setByteAddr(int num, InetAddress addr){

		byte[] re = ByteBuffer.allocate(num).put(addr.getAddress()).array();

		return re;
	}

	public static byte[] setByteTime(int num, long time){

		byte[] re = ByteBuffer.allocate(num).putLong(time).array();

		return re;
	}

	public static byte[] set2Byte(int pin){

		byte[] re = new byte[2];

		re[0] = (byte)(pin & 0xFF);
		re[1] = (byte)((pin >> 8) & 0xFF);

		return re;
	}

	// taken from http://stackoverflow.com/questions/140131/convert-a-string-representation-of-a-hex-dump-to-a-byte-array-using-java 
	public static String byteArrayToHex(byte[] array) {
		return DatatypeConverter.printHexBinary(array);
	}

	public static byte[] hexToByteArray(String s) {
		return DatatypeConverter.parseHexBinary(s);
	}
	
}