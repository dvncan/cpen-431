package com.s59906157.CPEN431.A1;

import java.io.*;
import java.net.DatagramPacket;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Arrays;

import javax.xml.bind.DatatypeConverter;


public class client{


	public static void main(String[] args){

		DatagramSocket s = null;
		DatagramPacket packet;
		DatagramPacket reply = null;
		//1381632
		int msg = 59906157;
		String addrIP = "162.219.6.226";
		int port = 5627;
		int counter = 0;
		int timeout = 1000;
		InetAddress addr;
		InetAddress addrIPsend;
		byte[] sendmsg = new byte[1024];
		boolean pktRecieved = false;

		System.out.println("Client initiated.");
		int randomPIN = (int)(Math.random()*9000)+1000;
		long time = System.currentTimeMillis();

		try{

			while(counter < 4 && (pktRecieved == false)){

				System.out.println("Attempt number:" + (counter+1));

				System.out.println("Creating header: ");

				addr = InetAddress.getByName("154.5.171.209");
				addrIPsend = InetAddress.getByName(addrIP);
				


				byte[] ipAddress = ByteBuffer.allocate(4).put(addr.getAddress()).array();
				byte[] prt = new byte[2];
				// 4 digits -> 2bytes
				prt[0] = (byte) (port & 0xFF);
				prt[1] = (byte) ((port >> 8) & 0xFF);

				byte[] rPin = new byte[2];
				// 4 digits -> 2bytes
				rPin[0] = (byte) (randomPIN & 0xFF);
				rPin[1] = (byte) ((randomPIN >> 8) & 0xFF);

				byte[] timeStamp = ByteBuffer.allocate(8).putLong(time).array();			

				byte[] Smsg = ByteBuffer.allocate(4).putInt(msg).array();

				//System.out.println("" + ipAddress.length);
				
				System.arraycopy(ipAddress, 0, sendmsg, 0, ipAddress.length);
				System.arraycopy(prt, 0, sendmsg, 4, prt.length);
				System.arraycopy(rPin, 0, sendmsg, 6, rPin.length);
				System.arraycopy(timeStamp, 0, sendmsg, 8, timeStamp.length);
				System.arraycopy(Smsg, 0, sendmsg, 16, Smsg.length);

				System.out.println(Arrays.toString(sendmsg));

				//sendmsg = ByteOrder.reverse(sendmsg);

				s = new DatagramSocket();
				packet = new DatagramPacket(sendmsg, sendmsg.length, addrIPsend, port);

				s.send(packet);

				System.out.println("Sent.");

				byte[] buffer = new byte[1024];

				reply = new DatagramPacket(buffer, buffer.length);

				try{
					
					s.setSoTimeout(timeout);
					s.receive(reply);
					pktRecieved = true;
					
				}catch(SocketTimeoutException e){
					
					timeout = (timeout*2);
					counter++;
					
				}
			}
			
			System.out.println("reply recieved: " + Arrays.toString(reply.getData()));

			s.close();

			System.out.println("close");
			
			byte[] replyArray = new byte[1024];
			
			replyArray = reply.getData();
			
			String replyHex = byteArrayToHex(reply.getData());
			
			System.out.println(replyHex);
			
			//int secretID = ByteOrder.beb2int(replyArray, 8);
			int hexNum = ByteOrder.leb2int(replyArray, 16);
			
			
			
			System.out.println("Sending ID: " + msg);
			
			System.out.println("Secret Code Length: " + hexNum);
			
			int start = 40;
			
			String shortReply = replyHex.substring(start, start+hexNum*2);
			
			System.out.println("Secret Code: " + shortReply);
			
//			replyArray = ByteOrder.reverse(replyArray);
//			
//			
//			
//			ByteBuffer bb = ByteBuffer.wrap(replyArray);
//			
//			ByteBuffer bb = ByteBuffer.wrap(replyHex.getBytes(), 16, 8);
//			
//			System.out.println(replyHex);
//			System.out.println(Arrays.toString(bb.array()));
//			
//			byte b;
//			
//			b = bb.get();
//			
//			System.out.println(bb.position() + "");
//			
//			int hexNum = 
//			
//			System.out.println(hexNum + "");
//			
//			System.out.println(shortReply);
//			
//			String hexNum = shortReply.substring(0, 8);
//			
//			System.out.println(hexNum);
			
			
			

		}catch(Exception e){

		}


	}


	// taken from http://stackoverflow.com/questions/140131/convert-a-string-representation-of-a-hex-dump-to-a-byte-array-using-java 
	public static String byteArrayToHex(byte[] array) {
		return DatatypeConverter.printHexBinary(array);
	}

	public static byte[] hexToByteArray(String s) {
		return DatatypeConverter.parseHexBinary(s);
	}

}

